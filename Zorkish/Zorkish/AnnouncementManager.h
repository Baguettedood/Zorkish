#pragma once

#include <vector>
#include <unordered_map>
#include <string>

class Message;
class ZorkishEntity;

class AnnouncementManager
{
public:
	AnnouncementManager();
	~AnnouncementManager();

	//Registers an entity with a given name. Sends messages to the specified entity if they have the given name as a recipient.
	void Register(std::string AnnouncementName, ZorkishEntity* EntityToAnnounceTo);
	void Deregister(std::string AnnouncementName, ZorkishEntity* EntityToStopAnnouncingTo);

	void Announce(Message* NewAnnouncement);

	static AnnouncementManager* GetInstance() { return instance; }

protected:

private:
	std::unordered_map<std::string, std::vector<ZorkishEntity*>*> AnnouncementList;

	std::vector<ZorkishEntity*>* GetRegisteredEntities(std::string AnnouncementName);
	unsigned int FindElementIndex(std::vector<ZorkishEntity*>* VectorToSearch, ZorkishEntity* ElementToFind);

	static AnnouncementManager* instance;
};

