#pragma once
#include <string>
#include <unordered_map>

class Message;

class Blackboard
{
public:
	Blackboard();
	~Blackboard();


	static Blackboard* GetInstance() { return instance; }

	void PutMessage(Message*);
	Message* GetMessage(std::string);

protected:

private:
	static Blackboard* instance;
	std::unordered_map<std::string, Message*> Messages;
};

