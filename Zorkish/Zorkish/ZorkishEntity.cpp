#include <iostream>
#include "ZorkishEntity.h"
#include "ZorkishComponent.h"
#include "Messages/Message.h"
#include "AnnouncementManager.h"
#include "Blackboard.h"

ZorkishEntity::ZorkishEntity(ZorkishEntity* newParent, std::string aName, std::string aDescription)
{
	Parent = newParent;
	Name = aName;
	Description = aDescription;

	AnnouncementManager::GetInstance()->Register("tick", this);
}

ZorkishEntity::ZorkishEntity(std::string aName, std::string aDescription) : 
	ZorkishEntity(nullptr, aName, aDescription)
{}

ZorkishEntity::~ZorkishEntity()
{
	for(auto ComponentToDelete : Components)
	{
		delete ComponentToDelete.second;
	}

	AnnouncementManager::GetInstance()->Deregister("tick", this);
}

bool ZorkishEntity::SendMessage(Message* SentMessage)
{
	if(ReceiveMessage(SentMessage))
	{
		return true;
	}

	//If we get to here, then this entity doesn't have a suitable receiver.
	//Send it to this entity's components.
	for(auto ComponentToSend : Components)
	{
		if(ComponentToSend.second->SendMessage(SentMessage))
		{
			return true;
		}
	}


	//TODO consider if we should send the message to child entities.
	return false;
}

void ZorkishEntity::Examine()
{
	std::cout << Description << std::endl;
	if(Composition.size() > 0)
	{
		std::cout << "There's something inside..." << std::endl;
	}
}

void ZorkishEntity::AddComponent(ZorkishComponent* Component)
{
	if(Component)
	{
		std::string ComponentName = Component->GetName();

		if(Components[ComponentName])
		{
			std::cerr << "Overriding component: " << ComponentName << std::endl;
		}

		Components[ComponentName] = Component;
		Component->SetParent(this); //Safe to assume the component should only be in one place at a time.
	}
}

bool ZorkishEntity::ReceiveMessage(Message* SentMessage)
{
	//Test announcements and blackboards
	Message* TickNum = Blackboard::GetInstance()->GetMessage("ticknum");

	if(SentMessage->GetRecipient() == "tick")
	{
		std::cout << GetName() << ":\tTick! ";
		if(TickNum != nullptr)
		{
			std::cout << TickNum->GetInt();
		}
		std::cout << std::endl;
	}

	return false; //Extended by children.
}
