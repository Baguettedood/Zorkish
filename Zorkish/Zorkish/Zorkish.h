#pragma once

enum class ZorkStage
{
	Menu,
	About,
	Help,
	SelectAdventure,
	Gameplay,
	NewScore,
	HallOfFame,
	Quit
};

struct ZorkishScore; //Forward Declaration
class ZorkishGameplay;

class Zorkish
{
public:
	Zorkish();
	~Zorkish();

	void Start();
protected:

private:
	ZorkStage stage = ZorkStage::Menu;
	bool QuitGame = false;
	ZorkishGameplay* Game = nullptr;

	void EscOrEnter();
	void SelectStage();
	void SelectAdventure();
	void Menu();
	void NewHighScore(const ZorkishScore Score);
	void HallOfFame();
	void Help();
	void About();


	void Quit();


};

