#include <iostream>
#include "Damage.h"
#include "Health.h"
#include "../ZorkishEntity.h"
#include "../Messages/Message.h"

Damage::Damage(int NewPower) : ZorkishComponent("damage")
{
	AttackPower = NewPower;
}


Damage::~Damage()
{
}

void Damage::Attack(ZorkishEntity* Target)
{
	if(!Parent)
	{
		std::cout << "Parent not found" << std::endl;
		return;
	}
	else if(!Target)
	{
		std::cout << "Target not found" << std::endl;
	}

	//Health* TargetHealth = (Health*)Target->GetComponent("health");
	Message AttackMessage = Message("health", "damage", AttackPower);
	if(Target->SendMessage(&AttackMessage))
	{
		std::cout << Parent->GetName() << " hits " << Target->GetName() << " for " << AttackPower << " damage!" << std::endl;
		//TargetHealth->Damage(AttackPower);
	}
	else
	{
		std::cout << "The " << Target->GetName() << " is immortal!" << std::endl;
	}
}
