#pragma once

#include "../ZorkishComponent.h"
#include <string>

class Damage : public ZorkishComponent
{
public:
	Damage(int NewPower);
	~Damage();

	void Attack(ZorkishEntity* Target);

protected:

private:
	int AttackPower = 10;

};

