#pragma once
#include <string>
#include "../ZorkishComponent.h"

class Health: public ZorkishComponent
{
public:
	Health(int StartHealth);
	~Health();

	int GetHealth() { return CurrentHealth; }
	int Damage(int DamageAmount);
	int Heal(int HealAmount);
	
	
protected:

	virtual bool ReceiveMessage(Message* SentMessage) override;

private:
	int CurrentHealth = 10;
	int MaxHealth = 10;

	int ChangeHealth(int DeltaHealth);
};

