#include "Health.h"
#include <iostream>
#include "../ZorkishEntity.h"

Health::Health(int StartHealth): ZorkishComponent("health")
{
	CurrentHealth = StartHealth;
	MaxHealth = StartHealth;
}


Health::~Health()
{
}

int Health::Damage(int DamageAmount)
{
	//Don't damage for negative health.
	if(DamageAmount <= 0)
		return CurrentHealth;

	return ChangeHealth(-DamageAmount);
}

int Health::Heal(int HealAmount)
{
	if(HealAmount <= 0)
		return CurrentHealth;

	return ChangeHealth(HealAmount);
}

bool Health::ReceiveMessage(Message* SentMessage)
{
	std::string FunctionToCall = SentMessage->GetFunction();
	int MessageValue = SentMessage->GetInt();

	if(FunctionToCall == "damage")
	{
		Damage(MessageValue);
		return true;
	}
	else if(FunctionToCall == "heal")
	{
		Heal(MessageValue);
		return true;
	}


	return false;
}

int Health::ChangeHealth(int DeltaHealth)
{
	if(CurrentHealth == 0)
	{
		//Already dead.
		std::cout << "The " << Parent->GetName() << " is already dead." << std::endl;
		return 0;
	}

	CurrentHealth += DeltaHealth;

	//Manual clamping since clamp isn't part of C++ until C++17.
	if(CurrentHealth < 0)
		CurrentHealth = 0;

	if(CurrentHealth > MaxHealth)
		CurrentHealth = MaxHealth;

	if(CurrentHealth == 0)
	{
		//Dead after damage
		//TODO tell parent that we should be dead.
		//Death Message?
	}

	return CurrentHealth;
}
