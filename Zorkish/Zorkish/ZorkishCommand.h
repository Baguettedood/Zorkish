#pragma once
#include <string>
#include <vector>

class ZorkishPlayer;


enum class ZorkishCommandResult
{
	OK,
	Error, //Something went wrong with the command
	Input_Error, //User didn't input all the data
	EndGame,
	HiScore
};

class ZorkishCommand
{
public:
	ZorkishCommand() {}
	~ZorkishCommand() {}

	//Send the full command string to this ZorkishCommand object.
	void GiveCommand(std::string Command) { CommandGiven = Command; }

	const std::string GetCommandName() const { return CommandName; }
	const std::string GetUsage() const { return Usage; }
	const std::vector<std::string> GetAliases() const { return Aliases; }

	ZorkishCommandResult Run(ZorkishPlayer* Player);
protected:
	std::string CommandName;
	std::vector<std::string> Aliases;

	std::string CommandGiven = "";
	std::string Usage = "";

	virtual ZorkishCommandResult Execute(ZorkishPlayer* Player) = 0;
	std::string TrimToParameters(std::string FullCommand);

	// http://stackoverflow.com/questions/236129/split-a-string-in-c/236803#236803
	//Splits a string into a vector by its delimiter (e.g. ' ').
	std::vector<std::string> Split(const std::string &s, char delim);

private:
	void Split(const std::string &s, char delim, std::vector<std::string> &elems);
};

