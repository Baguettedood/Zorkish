#include "ZorkishComposite.h"
#include "ZorkishEntity.h"
#include "ZorkishItem.h"
#include <iterator>
#include <iostream>

ZorkishComposite::ZorkishComposite() {}
ZorkishComposite::~ZorkishComposite()
{
	//std::vector::~vector() does not destroy items if they are pointers.
	//We can assume if they are still in the inventory, they shouldn't be anywhere else.
	for(ZorkishEntity* item : Items)
	{
		delete item;
	}
}

void ZorkishComposite::CreateEntity(std::string ItemName, std::string ItemDesc)
{
	ZorkishEntity* NewItem = new ZorkishEntity(ItemName, ItemDesc);
	Add(NewItem);
}

void ZorkishComposite::Add(ZorkishEntity* ItemToAdd)
{
	if(ItemToAdd == nullptr) { return; }

	//Ensure we haven't already picked up this item
	for(ZorkishEntity* item : Items)
	{
		if(ItemToAdd == item)
		{
			std::cout << "You already have that item!" << std::endl;
			return;
		}
	}

	Items.push_back(ItemToAdd);
}

void ZorkishComposite::DestroyItem(ZorkishEntity* ItemToDestroy)
{
	if(ItemToDestroy == nullptr) { return; }

	//Assume that the item only exists once.
	for(unsigned int i = 0; i < Items.size(); i++)
	{
		if(Items[i] == ItemToDestroy)
		{

			Items.erase(Items.begin() + i);
			delete ItemToDestroy;
			break;
		}
	}
}

void ZorkishComposite::DestroyItem(std::string ItemToDestroy)
{
	ZorkishEntity* FoundItem = FindItem(ItemToDestroy);
	DestroyItem(FoundItem);
}

ZorkishEntity* ZorkishComposite::FindItem(std::string ItemToFind)
{
	for(ZorkishEntity* item : Items)
	{
		if(item->GetName() == ItemToFind)
		{
			return item;
		}
	}

	return nullptr;
}

ZorkishEntity* ZorkishComposite::TakeItem(std::string ItemToFind)
{
	//Assume object only exists once.
	ZorkishEntity* ItemToTake = FindItem(ItemToFind);

	if(ItemToTake == nullptr)
	{
		return nullptr;
	}

	for(unsigned int i = 0; i < Items.size(); i++)
	{
		if(Items[i] == ItemToTake)
		{
			Items.erase(Items.begin() + i);
			return ItemToTake;
		}
	}

	//Code should be unreachable unless something has gone wrong.
	std::cerr << "Warning: Didn't take an item that we found: " << ItemToFind << std::endl;
	return nullptr;
}
