#include "AnnouncementManager.h"
#include "Messages/Message.h"
#include "ZorkishEntity.h"

AnnouncementManager* AnnouncementManager::instance = nullptr;

AnnouncementManager::AnnouncementManager()
{
	instance = this;
}


AnnouncementManager::~AnnouncementManager()
{
	if(instance == this)
	{
		instance = nullptr;
	}

	for(auto VectorToDelete : AnnouncementList)
	{
		delete VectorToDelete.second;
	}
}

void AnnouncementManager::Register(std::string AnnouncementName, ZorkishEntity* EntityToAnnounceTo)
{
	std::vector<ZorkishEntity*>* RegisteredEntities = GetRegisteredEntities(AnnouncementName);
	
	//Prevent duplicates registering for the same announcement.
	if(FindElementIndex(RegisteredEntities, EntityToAnnounceTo) == -1)
	{
		RegisteredEntities->push_back(EntityToAnnounceTo);
	}

}

void AnnouncementManager::Deregister(std::string AnnouncementName, ZorkishEntity* EntityToStopAnnouncingTo)
{
	std::vector<ZorkishEntity*>* RegisteredEntities = GetRegisteredEntities(AnnouncementName);
	unsigned int idx = FindElementIndex(RegisteredEntities, EntityToStopAnnouncingTo);

	//Only deregister if we ever actually registered.
	if(idx != -1)
	{
		RegisteredEntities->erase(RegisteredEntities->begin() + idx);
	}
}

void AnnouncementManager::Announce(Message* NewAnnouncement)
{
	std::string Recipient = NewAnnouncement->GetRecipient();

	auto RegisteredEntities = GetRegisteredEntities(Recipient);

	for(auto Entity : *RegisteredEntities)
	{
		Entity->SendMessage(NewAnnouncement);
	}
}

std::vector<ZorkishEntity*>* AnnouncementManager::GetRegisteredEntities(std::string AnnouncementName)
{
	std::vector<ZorkishEntity*>* RegisteredEntities = AnnouncementList[AnnouncementName];
	if(RegisteredEntities == nullptr)
	{
		RegisteredEntities = new std::vector<ZorkishEntity*>();
		AnnouncementList[AnnouncementName] = RegisteredEntities;
	}

	return RegisteredEntities;
}

unsigned int AnnouncementManager::FindElementIndex(std::vector<ZorkishEntity*>* VectorToSearch, ZorkishEntity* ElementToFind)
{
	//Assumes only one element.
	for(unsigned int i = 0; i < VectorToSearch->size(); i++)
	{
		if(VectorToSearch->at(i) == ElementToFind)
		{
			return i;
		}
	}

	return -1; //Not found
}
