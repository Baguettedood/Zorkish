#include "Zorkish.h"
#include <string>
#include <iostream>
#include <Windows.h>
#include "ZorkishScore.h"
#include "ZorkishGameplay.h"

Zorkish::Zorkish()
{
	Game = new ZorkishGameplay();
}
Zorkish::~Zorkish()
{
	delete Game;
}

void Zorkish::Start()
{
	stage = ZorkStage::Menu;
	SelectStage();
}

/* Loops to select the appropriate action based on the stage, or exits when the game is quitting. */
void Zorkish::SelectStage()
{
	while (!QuitGame)
	{
		switch (stage)
		{
		case ZorkStage::Menu:
			Menu();
			break;
		case ZorkStage::SelectAdventure:
			SelectAdventure();
			break;
		case ZorkStage::HallOfFame:
			HallOfFame();
			break;
		case ZorkStage::Help:
			Help();
			break;
		case ZorkStage::About:
			About();
			break;
		case ZorkStage::Quit:
			Quit();
			break;
		default:
			std::cerr << "Unimplemented stage" << std::endl;
			stage = ZorkStage::Menu;
			break;
		}
	}
}

void Zorkish::SelectAdventure()
{
	bool ValidAdventure = false;
	std::string AdventureFile;

	while (!ValidAdventure)
	{
		std::cout << "Zorkish :: Select Adventure" << std::endl;
		std::cout << "--------------------------------------------------------" << std::endl << std::endl;

		//std::cout << "Available Adventures:" << std::endl;
		
		//std::cout << "\t" << "1. Void World" << std::endl; //TODO Load adventures from file

		//std::cout << "The choice is made for you..." << std::endl << std::endl;


		std::cout << "Enter adventure file: ";
		std::getline(std::cin, AdventureFile);

		//TODO get input and validate it is in a proper range.
		ValidAdventure = true;
	}

	//TODO switch to select adventure

	//ZorkishGameplay* Game; //Pointer because destructors were happening when it was still in scope.
	delete Game;

	if(AdventureFile.length() != 0)
	{
		Game = new ZorkishGameplay(AdventureFile);
	}
	else
	{
		Game = new ZorkishGameplay();
	}


	ZorkGameResult GameResult = Game->Play();
	const ZorkishScore GameScore = Game->GetScore(); //Shouldn't be modified by this class.

	switch (GameResult)
	{
	case ZorkGameResult::HighScore:
		//Add high score
		stage = ZorkStage::NewScore;
		NewHighScore(GameScore);
		break;
	case ZorkGameResult::NoScore:
		stage = ZorkStage::Menu;
		break;
	default:
		stage = ZorkStage::Menu;
		break;
	}
}

void Zorkish::Menu()
{
	std::cout << "Zorkish :: Main Menu" << std::endl;
	std::cout << "--------------------------------------------------------" << std::endl << std::endl;

	std::cout << "Welcome to Zorkish Adventures" << std::endl << std::endl;

	std::cout << "\t1. Select Adventure and Play" << std::endl;
	std::cout << "\t2. Hall of Fame" << std::endl;
	std::cout << "\t3. Help" << std::endl;
	std::cout << "\t4. About" << std::endl;
	std::cout << "\t5. Quit" << std::endl;

	std::cout << std::endl << "Select 1-5:> ";
	std::string input;
	std::getline(std::cin, input);
	
	int selection = atoi(&input[0]);

	switch (selection)
	{
	case 1:
		stage = ZorkStage::SelectAdventure;
		break;
	case 2:
		stage = ZorkStage::HallOfFame;
		break;
	case 3:
		stage = ZorkStage::Help;
		break;
	case 4:
		stage = ZorkStage::About;
		break;
	case 5:
		stage = ZorkStage::Quit;
		break;
	default:
		break;
	}

}

void Zorkish::NewHighScore(ZorkishScore Score)
{
	std::cout << "Zorkish :: New High Score" << std::endl;
	std::cout << "--------------------------------------------------------" << std::endl << std::endl;

	std::cout << "Congratulations!" << std::endl << std::endl;

	std::cout << "You have made it to the Zorkish Hall Of Fame" << std::endl << std::endl;

	std::cout << "Adventure: " << Score.Adventure << std::endl;
	std::cout << "Score: " << Score.Score << std::endl;
	std::cout << "Moves: " << Score.Moves << std::endl << std::endl;

	std::cout << "Please type your name and press enter:" << std::endl;
	std::cout << ":> ";
	std::string PlayerName;
	std::cin >> PlayerName;

	//TODO actually add to high scores list.
	
	stage = ZorkStage::HallOfFame;
}

void Zorkish::HallOfFame()
{
	//TODO an actual high scores list
	std::cout << "Zorkish :: Hall Of Fame" << std::endl;
	std::cout << "--------------------------------------------------------" << std::endl << std::endl;

	std::cout << "Top 10 Zorkish Adventure Champions:" << std::endl;
	
	std::cout << "\t" << "1. Fred, Mountain World, 5000" << std::endl;
	std::cout << "\t" << "2. Mary, Mountain World, 4000" << std::endl;
	std::cout << "\t" << "3. Joe, Water World, 3000" << std::endl;
	std::cout << "\t" << "4. Henry, Mountain World, 2000" << std::endl;
	std::cout << "\t" << "5. Susan, Mountain World, 1000" << std::endl;

	EscOrEnter();
	stage = ZorkStage::Menu;
}

void Zorkish::Help()
{
	Game->Help();
	EscOrEnter();
	stage = ZorkStage::Menu;
}

void Zorkish::About()
{
	std::cout << "Zorkish :: About" << std::endl;
	std::cout << "--------------------------------------------------------" << std::endl << std::endl;

	std::cout << "Written by: Matthew Wiebenga" << std::endl << std::endl;

	EscOrEnter();
	stage = ZorkStage::Menu;
}

void Zorkish::EscOrEnter()
{
	std::cout << "Press ESC or Enter to return to the Main Menu" << std::endl;
	//Give the user a moment to lift their hand off ENTER.
	while (GetAsyncKeyState(VK_RETURN)) {}

	//Wait until the user presses ESC or ENTER.
	while (!GetAsyncKeyState(VK_ESCAPE) && !GetAsyncKeyState(VK_RETURN)) {}

	//TODO fix bug where an extra newline is placed if the user presses enter.
}

void Zorkish::Quit()
{
	QuitGame = true;
}
