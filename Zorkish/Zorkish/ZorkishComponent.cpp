#include "ZorkishComponent.h"

#include <iostream>

ZorkishComponent::ZorkishComponent(std::string NewName, int NewValue, ZorkishEntity* NewParent)
{
	Name = NewName;
	ComponentValue = NewValue;
	Parent = NewParent;
}
ZorkishComponent::~ZorkishComponent(){}

void ZorkishComponent::SetParent(ZorkishEntity* NewParent)
{
	Parent = NewParent;
}

bool ZorkishComponent::SendMessage(Message* SentMessage)
{
	return ReceiveMessage(SentMessage);
}

bool ZorkishComponent::ReceiveMessage(Message* SentMessage)
{
	// If needed, consider using std::dynamic_cast<T>(SentMessage) for casting.
	return false;
}
