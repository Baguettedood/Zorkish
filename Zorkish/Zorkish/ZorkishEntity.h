#pragma once
#include <unordered_map>
#include "ZorkishComposite.h"
#include <string>

class ZorkishComponent;
class Message;

class ZorkishEntity
{
public:
	ZorkishEntity(ZorkishEntity* newParent, std::string aName, std::string aDescription);
	ZorkishEntity(std::string aName, std::string aDescription);

	~ZorkishEntity();

	ZorkishEntity* GetParent() { return Parent; }
	std::string GetName() { return Name; }
	std::string GetDescription() { return Description; }
	ZorkishComposite* GetComposite() { return &Composition; }

	//Calls ReceiveMessage on this entity and all children. Stops when the message is accepted.
	bool SendMessage(Message* SentMessage);

	void Examine();

	//Components
	void AddComponent(ZorkishComponent* Component);
	ZorkishComponent* GetComponent(std::string ComponentName) { return Components[ComponentName]; }


protected:
	ZorkishEntity* Parent = nullptr;
	ZorkishComposite Composition;

	std::string Name = "entity";
	std::string Description = "A generic object.";

	std::unordered_map<std::string, ZorkishComponent*> Components;

	//Receives a sent message. Returns true if message is accepted and dealt with, false otherwise.
	virtual bool ReceiveMessage(Message* SentMessage);

private:

};

