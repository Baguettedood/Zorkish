// main.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Zorkish.h"


void PlayGame()
{
	Zorkish game;
	game.Start();
}

int main()
{
	PlayGame();
    return 0;
}

