#pragma once
#include <vector>

class ZorkishEntity;

class ZorkishComposite
{
public:
	ZorkishComposite();
    ~ZorkishComposite();

	//Creates a new item and places it in this inventory.
	void CreateEntity(std::string ItemName, std::string ItemDesc);

	//Places an item in the inventory.
	void Add(ZorkishEntity* ItemToAdd);

	//Destroys an item from existence.
	void DestroyItem(ZorkishEntity* ItemToDestroy);
	void DestroyItem(std::string ItemToDestroy);

	//Returns an item without removing it from the inventory.
	ZorkishEntity* FindItem(std::string ItemToFind);

	//Removes an item from the inventory without deleting it.
	ZorkishEntity* TakeItem(std::string ItemToFind);

	const std::vector<ZorkishEntity*> GetAllItems() { return Items; }
	size_t size() { return Items.size(); }
protected:
private:
	std::vector<ZorkishEntity*> Items;
};