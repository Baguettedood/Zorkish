#include <string>
#include <map>
#include "json/json.h"
#include "ZorkishComposite.h"
#include "ZorkishRoom.h"
#include "ZorkishPlayer.h"

#pragma once
class ZorkishWorld
{
public:
	ZorkishWorld(std::string aWorldFile);
	ZorkishWorld(std::string aWorldName, std::string aWorldDescription);


	~ZorkishWorld();


	std::string GetWorldName() { return WorldName; }
	std::string GetWorldDescription() { return WorldDescription; }
	ZorkishComposite* GetInventory() { return &Inventory; }
	ZorkishRoom* GetStartRoom() { return StartRoom; }
	ZorkishPlayer* GetPlayer() { return &Player; }

protected:
private:
	std::string WorldName = "Void World";
	std::string WorldDescription = "You shouldn't be here... There is no end to the void world...";
	std::map<std::string, ZorkishRoom*> Rooms;

	void AddRoomContents(ZorkishRoom* aRoom, Json::Value RoomContents);
	void AddObjectContents(ZorkishEntity* aObject, Json::Value ObjectContents);
	void AddAttributes(Json::Value &lPlayerAttributes, ZorkishEntity* lEntity);

	ZorkishComposite Inventory;
	ZorkishRoom* StartRoom = nullptr;
	ZorkishPlayer Player;

	ZorkishComponent* CreateComponent(std::string lMember, Json::Value lAttribute);
};