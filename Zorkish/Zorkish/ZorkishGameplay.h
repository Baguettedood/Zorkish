#pragma once

#include "ZorkishScore.h"
#include "ZorkishPlayer.h"
#include "CommandManager.h"
#include "Blackboard.h"
#include "AnnouncementManager.h"

class ZorkishWorld;

enum class ZorkGameResult
{
	NoScore,
	HighScore
};

class ZorkishGameplay
{
public:
	ZorkishGameplay();
	ZorkishGameplay(std::string WorldFile);
	~ZorkishGameplay();

	ZorkGameResult Play();
	ZorkishScore GetScore() const;
	void Help();
protected:
private:
	ZorkishScore Score;
	ZorkishPlayer* Player;
	ZorkishWorld* World;
	CommandManager Commands;
	Blackboard GameBlackboard;
	AnnouncementManager Announcements;

	bool QuitGame = false;

	//TODO should this be part of ZorkishRoom?
	void DescribeRoom();
};

