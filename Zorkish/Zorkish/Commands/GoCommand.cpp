#include "GoCommand.h"
#include "../ZorkishPlayer.h"
#include "../ZorkishRoom.h"
#include <vector>
#include <string>
#include <iostream>


GoCommand::GoCommand()
{
	CommandName = "go";

	Aliases.push_back("move");
	Aliases.push_back("n");
	Aliases.push_back("ne");
	Aliases.push_back("e");
	Aliases.push_back("se");
	Aliases.push_back("s");
	Aliases.push_back("sw");
	Aliases.push_back("w");
	Aliases.push_back("nw");
}


GoCommand::~GoCommand() {}

ZorkishCommandResult GoCommand::Execute(ZorkishPlayer* Player)
{
	if(Player->GetCurrentRoom() == nullptr)
	{
		std::cout << "You aren't in a room! You can't go anywhere!" << std::endl;
		return ZorkishCommandResult::OK;
	}


	if(CommandGiven.length() <= 2)
	{
		GoInitials(Player);
	}
	else
	{
		std::vector<std::string> SplitCommand = Split(CommandGiven, ' ');

		if(SplitCommand.size() < 2) //Is it shorter than "go <direction>"?
		{
			std::cout << "No direction specified." << std::endl;
			return ZorkishCommandResult::OK;
		}

		std::string GoDirection = SplitCommand[1];

		GoToDirection(Player, GoDirection);
	}

	return ZorkishCommandResult::OK;
}

void GoCommand::GoToDirection(ZorkishPlayer* Player, std::string GoDirection)
{
	ZorkishRoom* CurrentRoom = Player->GetCurrentRoom();
	ZorkishRoom* NewRoom = CurrentRoom->FindRoomByDirection(GoDirection);

	if(NewRoom != nullptr)
	{
		Player->SetCurrentRoom(NewRoom);
		std::cout << "You are in: " << NewRoom->GetName() << std::endl;
		std::cout << NewRoom->GetDescription() << std::endl;
	}
	else if(BeFunny(GoDirection))
	{
		//Do nothing because you're hilarious
	}
	else
	{
		std::cout << "You can't go that way!" << std::endl;
	}
}

void GoCommand::GoInitials(ZorkishPlayer* Player)
{
	char Direction = CommandGiven[0];
	char Direction2 = '\0';

	if(CommandGiven.size() >= 2)
	{
		Direction2 = CommandGiven[1];
	}

	switch(Direction)
	{
	case 'n':
		if(Direction2 == 'w') //nw
		{
			GoToDirection(Player, "northwest");
		}
		else if (Direction2 == 'e') //ne
		{
			GoToDirection(Player, "northeast");
		}
		else
		{
			GoToDirection(Player, "north");
		}
		break;
	case 'e':
		GoToDirection(Player, "east");
		break;
	case 'w':
		GoToDirection(Player, "west");
		break;
	case 's':
		if(Direction2 == 'w')
		{
			GoToDirection(Player, "southwest");
		}
		else if(Direction2 == 'e')
		{
			GoToDirection(Player, "southeast");
		}
		else
		{
			GoToDirection(Player, "south");
		}
		break;
	default:
		std::cout << "Unknown direction: " << Direction << std::endl;
		break;
	}
}

bool GoCommand::BeFunny(std::string GoDirection)
{
	if(GoDirection == "bonkers")
	{
		std::cout << "You went bonkers. It didn't help..." << std::endl;
		return true;
	}

	return false;
}
