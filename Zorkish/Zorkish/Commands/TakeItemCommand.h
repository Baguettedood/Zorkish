#pragma once
#include "../ZorkishCommand.h"

class ZorkishPlayer;
class ZorkishComposite;
class ZorkishEntity;
class ZorkishRoom;

class TakeItemCommand : public ZorkishCommand
{
public:
	TakeItemCommand();
	~TakeItemCommand();

protected:
	ZorkishCommandResult Execute(ZorkishPlayer* Player);
	ZorkishEntity* TakeItemFromRoom(ZorkishRoom* Room, std::string ItemToTake);

	ZorkishEntity* TakeItemFromInventory(ZorkishComposite* Inventory, std::string ItemToTake);
	ZorkishEntity* FindParentObject(ZorkishPlayer* Player, std::string ParentName);

private:
	int PlayerCarryingCapacity = 0;

};

