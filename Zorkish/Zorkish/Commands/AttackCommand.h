#pragma once
#include "../ZorkishCommand.h"

class ZorkishPlayer;
class ZorkishEntity;

class AttackCommand: public ZorkishCommand
{
public:
	AttackCommand();
	~AttackCommand();

protected:
	ZorkishCommandResult Execute(ZorkishPlayer* Player);
private:
	ZorkishEntity* FindTarget(std::string TargetName, ZorkishPlayer* Player);
};

