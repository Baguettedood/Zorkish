#pragma once
#include "../ZorkishCommand.h"
class DestroyItemCommand: public ZorkishCommand
{
public:
	DestroyItemCommand();
	~DestroyItemCommand();

private:
	ZorkishCommandResult Execute(ZorkishPlayer* Player);
};

