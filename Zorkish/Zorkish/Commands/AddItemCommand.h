#pragma once
#include "../ZorkishCommand.h"

class AddItemCommand: public ZorkishCommand
{
public:
	AddItemCommand();
	~AddItemCommand();

private:
	ZorkishCommandResult Execute(ZorkishPlayer* Player);
};

