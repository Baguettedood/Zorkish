#include "QuitCommand.h"


QuitCommand::QuitCommand()
{
	CommandName = "quit";

	Aliases.push_back("q");
	Aliases.push_back("exit");
}

ZorkishCommandResult QuitCommand::Execute(ZorkishPlayer* Player)
{
	return ZorkishCommandResult::EndGame;
}
