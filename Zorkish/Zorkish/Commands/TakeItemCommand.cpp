#include "TakeItemCommand.h"
#include <iostream>
#include "../ZorkishPlayer.h"
#include "../ZorkishComposite.h"
#include "../ZorkishRoom.h"
#include "../ZorkishEntity.h"
#include "../ZorkishComponent.h"


TakeItemCommand::TakeItemCommand()
{
	CommandName = "take";
	Aliases.push_back("pick"); //e.g. pick up
}

TakeItemCommand::~TakeItemCommand() {}

ZorkishCommandResult TakeItemCommand::Execute(ZorkishPlayer* Player)
{
	if(Player)
	{
		ZorkishComponent* Capacity = Player->GetComponent("capacity");
		if(Capacity)
		{
			PlayerCarryingCapacity = Capacity->GetValue();
		}
		else
		{
			PlayerCarryingCapacity = 0;
		}
	}
	else
	{
		return ZorkishCommandResult::Error;
	}

	// TODO give Items their own "Take" output.
	std::vector<std::string> SplitCommand = Split(CommandGiven, ' ');

	if(SplitCommand.size() >= 2 && SplitCommand[0] == "pick" && SplitCommand[1] == "up")
	{
		//Make things easier.
		SplitCommand.erase(SplitCommand.begin() + 1); // Erase "up" - (cue Rick Astley)
	}
	else if(SplitCommand[0] == "pick")
	{
		//i.e. "pick" but not "pick up"
		std::cout << "I don't understand." << std::endl;
		return ZorkishCommandResult::Input_Error;
	}

	// Determine if it's just "take x" or "take x from y"
	
	if(SplitCommand.size() == 1)
	{
		std::cout << "I don't know what to take." << std::endl;
		return ZorkishCommandResult::Input_Error;
	}

	if(SplitCommand.size() == 2)
	{
		//e.g. take x
		ZorkishEntity* TakenItem = TakeItemFromRoom(Player->GetCurrentRoom(), SplitCommand[1]);
		if(TakenItem != nullptr)
		{
			Player->GetComposite()->Add(TakenItem);
			std::cout << "You took the " << TakenItem->GetName() << "." << std::endl;
		}
		return ZorkishCommandResult::OK;
	}

	if(SplitCommand.size() >= 4)
	{
		//e.g. take x from y

		//Find the parent object (could be player or room)
		ZorkishEntity* ParentObject = FindParentObject(Player, SplitCommand[3]);
		//If it's found
		if(ParentObject && ParentObject->GetComposite())
		{
			//Find the child object
			ZorkishComposite* ParentInventory = ParentObject->GetComposite();
			//Take it.
			ZorkishEntity* TakenItem = TakeItemFromInventory(ParentInventory, SplitCommand[1]);
			//Put it in player
			if(TakenItem)
			{
				Player->GetComposite()->Add(TakenItem);
				std::cout << "You took the " << TakenItem->GetName() << " from the " << ParentObject->GetName() << "." << std::endl;
			}

			return ZorkishCommandResult::OK;
		}

	}

	return ZorkishCommandResult::OK;
}

ZorkishEntity* TakeItemCommand::TakeItemFromRoom(ZorkishRoom* Room, std::string ItemToTake)
{
	ZorkishComposite* RoomInventory = Room->GetInventory();
	return TakeItemFromInventory(RoomInventory, ItemToTake);
}

ZorkishEntity* TakeItemCommand::TakeItemFromInventory(ZorkishComposite* Inventory, std::string ItemToTake)
{
	if(Inventory == nullptr)
		return nullptr;

	bool ItemFound = false;
	bool CanTake = false;
	ZorkishEntity* FoundItem = Inventory->FindItem(ItemToTake);
	if(FoundItem)
	{
		ItemFound = true;
		ZorkishComponent* ItemWeight = FoundItem->GetComponent("weight");
		if(ItemWeight)
		{
			int Weight = ItemWeight->GetValue();
			if(Weight <= PlayerCarryingCapacity)
			{
				CanTake = true;
			}
		}
	}



	if(CanTake)
	{
		ZorkishEntity* TakenItem = Inventory->TakeItem(ItemToTake);
		return TakenItem;
	}
	else
	{
		if(ItemFound) // && !CanTake
		{
			std::cout << "The " << ItemToTake << " is too heavy!" << std::endl;
		}
		else // !ItemFound
		{
			std::cout << "You can't find the " << ItemToTake << "." << std::endl;
		}

		return nullptr;
	}
}

ZorkishEntity* TakeItemCommand::FindParentObject(ZorkishPlayer* Player, std::string ParentName)
{
	if(Player == nullptr)
		return nullptr;

	ZorkishComposite* PlayerInventory = Player->GetComposite();
	ZorkishRoom* Room = Player->GetCurrentRoom();
	ZorkishComposite* RoomInventory = nullptr;

	if(Room)
		RoomInventory = Room->GetInventory();

	if(PlayerInventory)
	{
		ZorkishEntity* ParentItem = PlayerInventory->FindItem(ParentName);
		if(ParentItem)
			return ParentItem;

	}

	if(RoomInventory)
	{
		ZorkishEntity* ParentItem = RoomInventory->FindItem(ParentName);
		if(ParentItem)
			return ParentItem;
		//COULD return ParentItem here without an if; shouldn't make a difference.
	}

	return nullptr;
}
