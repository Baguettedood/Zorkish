#include "ExamineCommand.h"
#include "../ZorkishPlayer.h"
#include "../ZorkishRoom.h"
#include "../ZorkishItem.h"
#include <iostream>


ExamineCommand::ExamineCommand()
{
	CommandName = "examine";

	Aliases.push_back("look"); //For look/look at commands.
}

ExamineCommand::~ExamineCommand() {}

ZorkishCommandResult ExamineCommand::Execute(ZorkishPlayer* Player)
{
	std::vector<std::string> SplitCommand = Split(CommandGiven, ' ');

	//Is the command simply "look"?
	if((SplitCommand.size() == 1) && (SplitCommand[0] == "look"))
	{
		LookAroundRoom(Player->GetCurrentRoom());
	}
	else if(SplitCommand.size() == 2 && SplitCommand[0] == "look" && SplitCommand[1] == "around")
	{
		LookAroundRoom(Player->GetCurrentRoom());
	}
	else if(SplitCommand.size() >= 3 && SplitCommand[0] == "look" && SplitCommand[1] == "at")
	{
		//Could be "look at x" or "look at x in y"
		if(SplitCommand.size() == 5)
		{
			//look at x in y
			ExamineItemInAnotherItem(Player, SplitCommand[2], SplitCommand[4]);
		}
		else if(SplitCommand[2] == "room")
		{
			//or "look at room"?
			LookAroundRoom(Player->GetCurrentRoom());
		}
		else
		{
			//look at x
			Examine(Player, SplitCommand[2]);
		}
	}
	else if(SplitCommand.size() == 2 && SplitCommand[0] == "examine" && SplitCommand[1] == "room")
	{
		LookAroundRoom(Player->GetCurrentRoom());
	}
	else if(SplitCommand.size() >= 2 && SplitCommand[0] == "examine")
	{
		//Could be "examine x" or "examine x in y"

		if(SplitCommand.size() == 4)
		{
			//examine x in y
			//Assume the player isn't insane enough to do something like "look at key in bag in chest in closet"
			ExamineItemInAnotherItem(Player, SplitCommand[1], SplitCommand[3]);
		}
		else
		{
			//examine x
			Examine(Player, SplitCommand[1]);
		}

	}
	else if(SplitCommand.size() == 3 && SplitCommand[0] == "look" && SplitCommand[1] == "in")
	{
		//e.g. look in bag
		//List objects inside the other object
		ListObjectsInside(Player, SplitCommand[2]);
	}
	else
	{
		std::cout << "I don't understand..." << std::endl;
	}

	return ZorkishCommandResult::OK;
}

void ExamineCommand::Examine(ZorkishPlayer* Player, std::string Target)
{
	//std::cout << "TODO examine: " << Target << "." << std::endl;

	ZorkishRoom* PlayerRoom = Player->GetCurrentRoom();
	bool ItemFound = ExamineRoomForItem(PlayerRoom, Target);

	if(!ItemFound)
		ItemFound = ExaminePlayerForItem(Player, Target);

	//If we still haven't found it, it doesn't exist.
	if(!ItemFound)
	{
		std::cout << "I don't see a " << Target << " anywhere." << std::endl;
	}
}

void ExamineCommand::LookAroundRoom(ZorkishRoom* Room)
{
	if(Room == nullptr) { return; }

	std::cout << Room->GetDescription() << std::endl << std::endl;

	std::cout << "You look around the " << Room->GetName() << "." << std::endl;

	auto AllItems = Room->GetInventory()->GetAllItems();
	if(AllItems.size() > 0)
	{
		std::cout << "From the " << Room->GetName() << ", you can see:" << std::endl;
		for(ZorkishEntity* Entity : AllItems)
		{
			std::cout << "\t" << "a " << Entity->GetName() << std::endl;
			//std::cout << "An object!" << std::endl;
		}
	}

}

bool ExamineCommand::ExamineRoomForItem(ZorkishRoom* Room, std::string Target)
{
	if(Room == nullptr) { return false; }
	ZorkishComposite* RoomInventory = Room->GetInventory();
	if(RoomInventory == nullptr) { return false; }

	ZorkishEntity* FoundItem = RoomInventory->FindItem(Target);

	if(FoundItem != nullptr)
	{
		FoundItem->Examine();
		return true;
	}
	else
	{
		return false;
	}

	return false;
}

bool ExamineCommand::ExaminePlayerForItem(ZorkishPlayer* Player, std::string Target)
{
	if(Player == nullptr) { return false; }
	ZorkishComposite* PlayerInventory = Player->GetComposite();
	if(PlayerInventory == nullptr) { return false; }

	ZorkishEntity* FoundItem = PlayerInventory->FindItem(Target);

	if(FoundItem != nullptr)
	{
		FoundItem->Examine();
		return true;
	}
	else
	{
		return false;
	}

	return false;
}

void ExamineCommand::ExamineItemInAnotherItem(ZorkishPlayer* Player, std::string ItemToExamine, std::string ParentItem)
{
	//TODO lots of null pointer checking.

	ZorkishRoom* Room = Player->GetCurrentRoom();
	ZorkishComposite* RoomInventory = Room->GetInventory();
	ZorkishComposite* PlayerInventory = Player->GetComposite();

	bool FirstItemFound = false;
	//Check player inventory for item
	ZorkishEntity* FoundItem = PlayerInventory->FindItem(ParentItem);
	FirstItemFound = (FoundItem != nullptr);

	//If not found, check room inventory for item
	if(!FirstItemFound)
	{
		//Check room inventory
		FoundItem = RoomInventory->FindItem(ParentItem);
		FirstItemFound = (FoundItem != nullptr);
	}


	if(FirstItemFound)
	{
		ZorkishComposite* FirstItemInventory = FoundItem->GetComposite();

		ZorkishEntity* EntityToExamine = FirstItemInventory->FindItem(ItemToExamine);
		if(EntityToExamine != nullptr)
		{
			EntityToExamine->Examine();
		}
	}
}

void ExamineCommand::ListObjectsInside(ZorkishPlayer* Player, std::string ItemToLookInside)
{
	//Find the object in question
	ZorkishComposite* PlayerInventory = Player->GetComposite();

	ZorkishEntity* FoundItem = PlayerInventory->FindItem(ItemToLookInside);
	if(FoundItem == nullptr)
	{
		ZorkishComposite* RoomInventory = Player->GetCurrentRoom()->GetInventory();
		FoundItem = RoomInventory->FindItem(ItemToLookInside);
	}

	if(FoundItem != nullptr)
	{
		//List all objects inside.

		ZorkishComposite* ItemInventory = FoundItem->GetComposite();

		auto AllItems = ItemInventory->GetAllItems();
		if(AllItems.size() > 0)
		{
			std::cout << "Inside the " << FoundItem->GetName() << " you see:" << std::endl;
			for(ZorkishEntity* ThisEntity : AllItems)
			{
				std::cout << "\t" << ThisEntity->GetName() << std::endl;
			}
		}
		else
		{
			std::cout << "The " << FoundItem->GetName() << " is empty!" << std::endl;
		}
	}
	else
	{
		//Didn't find it.
		std::cout << "I can't find a " << ItemToLookInside << std::endl;
	}
}
