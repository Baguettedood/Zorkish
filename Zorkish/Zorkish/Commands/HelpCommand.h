#pragma once
#include "../ZorkishCommand.h"

class ZorkishPlayer;

class HelpCommand: public ZorkishCommand
{
public:
	HelpCommand(const std::vector<ZorkishCommand*>* ManagedCommands);
	~HelpCommand();

protected:
	ZorkishCommandResult Execute(ZorkishPlayer* Player);

private:

	const std::vector<ZorkishCommand*>* Commands;
};

