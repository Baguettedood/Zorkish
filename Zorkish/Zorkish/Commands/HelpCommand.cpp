#include "HelpCommand.h"
#include "../ZorkishPlayer.h"
#include <vector>
#include <iostream>


HelpCommand::HelpCommand(const std::vector<ZorkishCommand*>* ManagedCommands)
{
	Commands = ManagedCommands;

	CommandName = "help";

	Aliases.push_back("?");
}

HelpCommand::~HelpCommand() {}

ZorkishCommandResult HelpCommand::Execute(ZorkishPlayer* Player)
{
	//Loop through all commands
	std::cout << "Zorkish :: Help" << std::endl;
	std::cout << "--------------------------------------------------------" << std::endl << std::endl;

	std::cout << "The following commands are supported:" << std::endl;

	for(ZorkishCommand* CurrentCommand : *Commands)
	{
		if(CurrentCommand->GetUsage() == "")
		{
			std::cout << "\t" << CurrentCommand->GetCommandName() << std::endl;
		}
		else // Usage exists
		{
			std::cout << "\t" << CurrentCommand->GetUsage() << std::endl;
		}
	}

	return ZorkishCommandResult::OK;
}
