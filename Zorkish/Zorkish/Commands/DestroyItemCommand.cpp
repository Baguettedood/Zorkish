#include "DestroyItemCommand.h"
#include <string>
#include <iostream>
#include "../ZorkishComposite.h"
#include "../ZorkishPlayer.h"


DestroyItemCommand::DestroyItemCommand()
{
	CommandName = "destroy";
	Aliases.push_back("delete");
}


DestroyItemCommand::~DestroyItemCommand()
{
}

ZorkishCommandResult DestroyItemCommand::Execute(ZorkishPlayer* Player)
{
	//Assume it's in the form "destroy hat".
	std::string DestroyItemName = TrimToParameters(CommandGiven);

	//TODO consider checking room inventory for item to destroy

	ZorkishComposite* PlayerInventory = Player->GetComposite();
	PlayerInventory->DestroyItem(DestroyItemName);
	std::cout << "Destroyed " << DestroyItemName << "." << std::endl;

	return ZorkishCommandResult::OK;
}
