#pragma once
#include "../ZorkishCommand.h"

class HiScoreCommand: public ZorkishCommand
{
public:
	HiScoreCommand();
	~HiScoreCommand();

protected:
	ZorkishCommandResult Execute(ZorkishPlayer* Player);
};

