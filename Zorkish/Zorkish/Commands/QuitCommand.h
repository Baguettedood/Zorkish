#pragma once

#include "../ZorkishCommand.h"

class QuitCommand: public ZorkishCommand
{
public:
	QuitCommand();
	~QuitCommand() {}
protected:

private:
	//Tells the caller to quit the game.
	ZorkishCommandResult Execute(ZorkishPlayer* Player);
};

