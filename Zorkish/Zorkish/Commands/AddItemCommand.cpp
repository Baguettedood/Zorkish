#include "AddItemCommand.h"
#include <iostream>
#include "../ZorkishPlayer.h"
#include "../ZorkishComposite.h"


AddItemCommand::AddItemCommand()
{
	CommandName = "additem";

	Aliases.push_back("add");
	Aliases.push_back("insert");
}

AddItemCommand::~AddItemCommand()
{
}

ZorkishCommandResult AddItemCommand::Execute(ZorkishPlayer* Player)
{
	std::string ItemName;
	std::cout << "Enter item name: ";
	std::getline(std::cin, ItemName);

	std::string ItemDescription;
	std::cout << "Enter item description: ";
	std::getline(std::cin, ItemDescription);

	ZorkishComposite* Inventory = Player->GetComposite();
	Inventory->CreateEntity(ItemName, ItemDescription);

	return ZorkishCommandResult::OK;
}
