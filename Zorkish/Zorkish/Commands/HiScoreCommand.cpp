#include "HiScoreCommand.h"



HiScoreCommand::HiScoreCommand()
{
	CommandName = "hiscore";

	Aliases.push_back("highscore");
}

HiScoreCommand::~HiScoreCommand()
{
}

ZorkishCommandResult HiScoreCommand::Execute(ZorkishPlayer* Player)
{
	return ZorkishCommandResult::HiScore;
}
