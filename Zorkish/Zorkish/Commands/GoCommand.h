#pragma once
#include "../ZorkishCommand.h"
#include <string>

class ZorkishPlayer;


class GoCommand: public ZorkishCommand
{
public:
	GoCommand();
	~GoCommand();

protected:
	ZorkishCommandResult Execute(ZorkishPlayer* Player);


private:
	//Handles initial-go commands e.g. n, ne, w, sw, etc.
	void GoInitials(ZorkishPlayer* Player);
	void GoToDirection(ZorkishPlayer* Player, std::string GoDirection);
	bool BeFunny(std::string GoDirection);
};

