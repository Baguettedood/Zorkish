#include "AttackCommand.h"
#include <vector>
#include <iostream>
#include "../ZorkishPlayer.h"
#include "../ZorkishComponent.h"
#include "../ZorkishComposite.h"
#include "../Components/Damage.h"
#include "../Components/Health.h"
#include "../ZorkishRoom.h"


AttackCommand::AttackCommand()
{
	CommandName = "attack";

	Aliases.push_back("punch");
	Aliases.push_back("hit");
}


AttackCommand::~AttackCommand()
{
}

ZorkishCommandResult AttackCommand::Execute(ZorkishPlayer* Player)
{
	if(!Player)
		return ZorkishCommandResult::Error;
	std::vector<std::string> SplitCommand = Split(CommandGiven, ' ');
	//Find target
	ZorkishEntity* Target = FindTarget(SplitCommand[1], Player);
	//TODO find weapon
	//Attack target
	Damage* PlayerDamage = (Damage*)Player->GetComponent("damage");

	if(!PlayerDamage)
	{
		std::cout << "You're too weak to attack anything!" << std::endl;
		return ZorkishCommandResult::OK;
	}

	PlayerDamage->Attack(Target);

	return ZorkishCommandResult::OK;
}

ZorkishEntity* AttackCommand::FindTarget(std::string TargetName, ZorkishPlayer* Player)
{
	ZorkishRoom* Room = Player->GetCurrentRoom();
	if(!Room || !Room->GetInventory())
		return nullptr;

	ZorkishEntity* Target = Room->GetInventory()->FindItem(TargetName);
	return Target;
}
