#pragma once
#include "../ZorkishCommand.h"

class ZorkishPlayer;
class ZorkishRoom;

class ExamineCommand: public ZorkishCommand
{
public:
	ExamineCommand();
	~ExamineCommand();

protected:
	ZorkishCommandResult Execute(ZorkishPlayer* Player);

	//TODO refactor some of these to reduce repeated code.
	void Examine(ZorkishPlayer* Player, std::string Target);
	void LookAroundRoom(ZorkishRoom* Room);
	bool ExamineRoomForItem(ZorkishRoom* Room, std::string Target);
	bool ExaminePlayerForItem(ZorkishPlayer* Player, std::string Target);
	void ExamineItemInAnotherItem(ZorkishPlayer* Player, std::string ItemToExamine, std::string ParentItem);
	void ListObjectsInside(ZorkishPlayer* Player, std::string ItemToLookInside);

private:

};

