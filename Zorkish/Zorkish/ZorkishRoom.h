#include "ZorkishComposite.h"
#include <string>
#include <map>

#pragma once
class ZorkishRoom
{
public:
	ZorkishRoom();
	ZorkishRoom(std::string aRoomId, std::string aRoomName, std::string aRoomDescription);
	~ZorkishRoom();

	std::string GetName() { return RoomName; }
	std::string GetDescription() { return RoomDescription; }
	ZorkishComposite* GetInventory() { return &Inventory; }

	void AddConnection(std::string Direction, ZorkishRoom* Room);
	ZorkishRoom* FindRoomByDirection(std::string Direction);

protected:
private:
	std::string RoomId = "emptyroom";
	std::string RoomName = "Empty Room";
	std::string RoomDescription = "There's nothing in here...";
	std::map<std::string, ZorkishRoom*> Connections;

	ZorkishComposite Inventory;
};

