#pragma once

//#include "ZorkishCommand.h"
#include <unordered_map>
#include <vector>
#include <string>

class ZorkishCommand;

class CommandManager
{
public:
	CommandManager();
	~CommandManager();

	//Finds the appropriate command, gives it its parameters, and returns the command to the caller.
	ZorkishCommand* InputCommand(std::string GivenCommand);

	ZorkishCommand* GetHelp() { return Help; }
protected:


private:


	//Holds commands and aliases. May contain multiple references to one pointer due to aliases.
	std::unordered_map<std::string, ZorkishCommand*> CommandDictionary; 

	//Makes destruction easier if they're all in one place. Only contains one reference per instance.
	std::vector<ZorkishCommand*> AllCommands;

	//Make Help command easier to access from another class.
	ZorkishCommand* Help = nullptr;

	//Finds the first space in the command string and returns the command string before the space.
	//Returns whole command string if there is no space.
	std::string ToFirstSpace(std::string GivenCommand);

	void ConvertToLowercase(std::string &StringToConvert);

	//Populates the CommandDictionary with commands from AllCommands.
	void PopulateDictionary();

	//Checks if the element has already been assigned and assigns if it has not. Otherwise throws an error.
	void AddSingleElement(std::string, ZorkishCommand*);
};

