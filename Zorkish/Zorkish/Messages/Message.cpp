#include "Message.h"


Message::Message(std::string Recipient, std::string Function, int NewValue)
{
	_Recipient = Recipient;
	_Function = Function;
	IntValue = NewValue;
}

Message::~Message()
{
}

std::string Message::GetRecipient()
{
	return _Recipient;
}

std::string Message::GetFunction()
{
	return _Function;
}

int Message::GetInt()
{
	return IntValue;
}

void Message::SetInt(int NewValue)
{
	IntValue = NewValue;
}
