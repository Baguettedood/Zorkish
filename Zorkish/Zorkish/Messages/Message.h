#pragma once
#include <string>

/*
* https://www.hscripts.com/tutorials/cpp/cpp-message-passing.php
* In OOPs, Message Passing involves specifying the name of objects, the name of the function, and the information to be sent.
*/

class Message
{
public:
	//Recipient is currently unused.
	Message(std::string Recipient, std::string Function = "", int NewValue = 0);
	~Message();

	std::string GetRecipient();
	std::string GetFunction();

	int GetInt();
	void SetInt(int NewValue);
protected:

private:
	std::string _Recipient = "";
	std::string _Function = ""; //Function to call e.g. "hurt"
	int IntValue = 0;
	//Data handled by subclass?
	//Recipient, Reason (e.g. Damage, Healing, etc.), some sort of value.
};

