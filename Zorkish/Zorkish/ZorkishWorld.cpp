#include "ZorkishWorld.h"
#include <iostream>
#include <fstream>
#include "json/json.h" //Since I was almost writing JSON anyway.
#include "ZorkishEntity.h"
#include "ZorkishRoom.h"
#include "ZorkishComponent.h"

#include "Components/Health.h"
#include "Components/Damage.h"

ZorkishWorld::ZorkishWorld(std::string aWorldFile)
{
	//std::ifstream InputFile(aWorldFile, std::ifstream::in);
	std::ifstream InputFile;
	InputFile.open(aWorldFile, std::ifstream::binary|std::ifstream::in);
	if(!InputFile.is_open())
	{
		std::cout << "File could not be opened: " << aWorldFile << std::endl;
		return;
	}

	Json::Value root;
	InputFile >> root;

	std::string lWorldName = root["name"].asString();
	std::string lWorldDescription = root["description"].asString();

	WorldName = lWorldName;
	WorldDescription = lWorldDescription;


	//Rooms
	Json::Value lRooms = root["rooms"];

	for(Json::Value lRoom : lRooms)
	{
		std::string lRoomId = lRoom["room_id"].asString();
		std::string lRoomName = lRoom["name"].asString();
		std::string lRoomDesc = lRoom["description"].asString();

		ZorkishRoom* thisRoom = new ZorkishRoom(lRoomId, lRoomName, lRoomDesc);
		Rooms[lRoomId] = thisRoom;


		Json::Value RoomContents = lRoom["contents"];
		AddRoomContents(thisRoom, RoomContents);
	}

	//Rooms Connections
	Json::Value lConnections = root["roomconnections"];
	for(Json::Value lConnection : lConnections)
	{
		std::string lFrom = lConnection["from"].asString();
		std::string lTo = lConnection["to"].asString();
		std::string lDirection = lConnection["direction"].asString();

		ZorkishRoom* lFromRoom = Rooms[lFrom];
		ZorkishRoom* lToRoom = Rooms[lTo];

		lFromRoom->AddConnection(lDirection, lToRoom);
	}


	//Player

	Json::Value lPlayer = root["player"];
	Json::Value lPlayerAttributes = lPlayer["attributes"];
	AddAttributes(lPlayerAttributes, &Player);

	std::string lStartRoom = root["startroom"].asString();
	StartRoom = Rooms[lStartRoom];
	Player.SetCurrentRoom(StartRoom);
}

void ZorkishWorld::AddAttributes(Json::Value &lPlayerAttributes, ZorkishEntity* lEntity)
{
	for(Json::Value lAttribute : lPlayerAttributes)
	{
		auto MemberNames = lAttribute.getMemberNames();
		for(auto lMember : MemberNames)
		{
			auto lComponent = CreateComponent(lMember, lAttribute[lMember]);
			if(lComponent)
			{
				lEntity->AddComponent(lComponent);
			}
		}
	}
}

ZorkishWorld::ZorkishWorld(std::string aWorldName, std::string aWorldDescription)
{
	WorldName = aWorldName;
	WorldDescription = aWorldDescription;
}

ZorkishWorld::~ZorkishWorld()
{
	//Delete all rooms.
	for(auto It = Rooms.begin(); It != Rooms.end(); ++It)
	{
		delete It->second;
	}
}

void ZorkishWorld::AddRoomContents(ZorkishRoom* aRoom, Json::Value RoomContents)
{
	for(Json::Value RoomObject : RoomContents)
	{
		//Determine type of object

		std::string ObjectName = RoomObject["name"].asString();
		std::string ObjectDesc = RoomObject["description"].asString();
		//Create object
		ZorkishEntity* NewEntity = new ZorkishEntity(ObjectName, ObjectDesc); //TODO figure out if object should have room parent.
		//Add object's contents (Put in its own method? It could go all the way down)
		Json::Value ObjectContents = RoomObject["contents"];
		AddObjectContents(NewEntity, ObjectContents);

		//Add attributes and actions
		Json::Value ObjectAttributes = RoomObject["attributes"];
		AddAttributes(ObjectAttributes, NewEntity);


		//Add it to room
		aRoom->GetInventory()->Add(NewEntity);
	}
}

void ZorkishWorld::AddObjectContents(ZorkishEntity* aObject, Json::Value ObjectContents)
{
	for(Json::Value ChildObject : ObjectContents)
	{
		std::string ObjectName = ChildObject["name"].asString();
		std::string ObjectDesc = ChildObject["description"].asString();
		ZorkishEntity* ChildEntity = new ZorkishEntity(aObject, ObjectName, ObjectDesc);
		Json::Value ChildObjectContents = ChildObject["contents"];
		AddObjectContents(ChildEntity, ChildObjectContents);

		Json::Value ChildObjectAttributes = ChildObject["attributes"];
		AddAttributes(ChildObjectAttributes, ChildEntity);

		aObject->GetComposite()->Add(ChildEntity);
	}
}

ZorkishComponent* ZorkishWorld::CreateComponent(std::string lMember, Json::Value lAttribute)
{
	ZorkishComponent* NewComponent = nullptr;
	if(lMember == "health")
	{
		//Do something about health component creation.
		NewComponent = new Health(lAttribute.asInt());
	}
	else if(lMember == "damage")
	{
		NewComponent = new Damage(lAttribute.asInt());
	}
	else
	{
		NewComponent = new ZorkishComponent(lMember, lAttribute.asInt());
	}

	return NewComponent;
}

