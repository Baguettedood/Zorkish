#pragma once

#include <string>

struct ZorkishScore
{
	std::string Adventure = "None";
	int Score = 0;
	int Moves = 0;
};