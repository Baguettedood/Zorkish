#include "Blackboard.h"
#include "Messages/Message.h"
#include <iostream>

Blackboard* Blackboard::instance = nullptr;

Blackboard::Blackboard()
{
	instance = this;
}

Blackboard::~Blackboard()
{
	if(instance == this)
	{
		instance = nullptr;
	}

	for(auto MessageToDelete : Messages)
	{
		//We will assume that messages aren't held by anything else by the time this destructor is called
		//and thus are out of scope
		//TODO really need some smart pointers
		delete MessageToDelete.second;
	}
}

void Blackboard::PutMessage(Message* MessageToPut)
{
	std::string Recipient = MessageToPut->GetRecipient();

	Message* ExistingMessage = Messages[Recipient];
	if(ExistingMessage != nullptr && ExistingMessage != MessageToPut)
	{
		std::cout << "Deleting message in " << Recipient << std::endl;
		delete ExistingMessage;
	}

	Messages[Recipient] = MessageToPut;
}

Message* Blackboard::GetMessage(std::string MessageToGet)
{
	//Let the other side handle nullptrs.
	return Messages[MessageToGet];
}
