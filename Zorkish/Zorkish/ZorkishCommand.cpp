#include "ZorkishCommand.h"
#include <sstream>


ZorkishCommandResult ZorkishCommand::Run(ZorkishPlayer* Player)
{
	ZorkishCommandResult Result = Execute(Player);
	CommandGiven = ""; //Reset command so the instance can be used again.
	return Result;
}

std::string ZorkishCommand::TrimToParameters(std::string FullCommand)
{
	unsigned int SpacePosition = 0;
	for(SpacePosition = 0; SpacePosition < FullCommand.length(); SpacePosition++)
	{
		if(FullCommand[SpacePosition] == ' ')
			break;
	}

	if(SpacePosition == FullCommand.length())
	{
		return FullCommand;
	}
	else
	{
		std::string CommandOnly = FullCommand.substr(SpacePosition + 1, std::string::npos);
		return CommandOnly;
	}
}

std::vector<std::string> ZorkishCommand::Split(const std::string &s, char delim)
{
	std::vector<std::string> elems;
	Split(s, delim, elems);
	return elems;
}

void ZorkishCommand::Split(const std::string &s, char delim, std::vector<std::string> &elems)
{
	std::stringstream ss;
	ss.str(s);
	std::string item;

	while(std::getline(ss, item, delim))
	{
		if(!item.empty())
		{
			elems.push_back(item);
		}
	}
}
