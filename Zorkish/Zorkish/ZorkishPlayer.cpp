#include "ZorkishPlayer.h"
#include "ZorkishComposite.h"
#include "ZorkishItem.h"
#include "ZorkishRoom.h"

ZorkishPlayer::ZorkishPlayer():ZorkishEntity("me", "It's me!")
{

}
ZorkishPlayer::~ZorkishPlayer(){}

void ZorkishPlayer::SetCurrentRoom(ZorkishRoom* NewRoom)
{
	if(NewRoom != nullptr)
	{
		CurrentRoom = NewRoom;
	}
}

