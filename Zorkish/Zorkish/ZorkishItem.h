#pragma once

#include <string>
#include "ZorkishEntity.h"

class ZorkishItem: public ZorkishEntity //TODO Remove this class?
{
public:
    ZorkishItem(std::string aName, std::string aDescription);
    ~ZorkishItem();

protected:
private:

};