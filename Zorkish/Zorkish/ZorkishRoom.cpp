#include "ZorkishRoom.h"

ZorkishRoom::ZorkishRoom(){}

ZorkishRoom::ZorkishRoom(std::string aRoomId, std::string aRoomName, std::string aRoomDescription)
{
	RoomId = aRoomId;
	RoomName = aRoomName;
	RoomDescription = aRoomDescription;
}

ZorkishRoom::~ZorkishRoom() {} //Rooms in Connections map are all destroyed by ZorkishWorld.cpp.

void ZorkishRoom::AddConnection(std::string Direction, ZorkishRoom* Room)
{
	//TODO consider validating whether or not this room is already in use.
	Connections[Direction] = Room;
}

ZorkishRoom* ZorkishRoom::FindRoomByDirection(std::string Direction)
{
	try
	{
		//Throws an exception if the room is not found.
		//[] operator may have created one.
		ZorkishRoom* NextRoom = Connections.at(Direction);
		return NextRoom;
	}
	catch(const std::exception&)
	{
		return nullptr; //i.e. no room found.
	}

}
