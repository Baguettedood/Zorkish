#pragma once
#include <string>
#include "Messages/Message.h"

class ZorkishEntity;


//Largely an abstract class.
class ZorkishComponent
{
public:
	ZorkishComponent(std::string NewName, int NewValue = 0, ZorkishEntity* NewParent = nullptr);
	~ZorkishComponent();

	std::string GetName() { return Name; }
	int GetValue() { return ComponentValue; }
	void SetParent(ZorkishEntity* NewParent);

	bool SendMessage(Message* SentMessage);
protected:
	std::string Name = "component";
	int ComponentValue = 0;
	ZorkishEntity* Parent = nullptr;

	virtual bool ReceiveMessage(Message* SentMessage);

private:
};

