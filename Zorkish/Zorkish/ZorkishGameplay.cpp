#include <iostream>
#include <string>
#include "ZorkishGameplay.h"
#include "ZorkishScore.h"
#include "ZorkishPlayer.h"
#include "ZorkishWorld.h"
#include "ZorkishRoom.h"
#include "ZorkishCommand.h"
#include "Messages/Message.h"

ZorkishGameplay::ZorkishGameplay()
{
	World = new ZorkishWorld("Void World", "This world is simple and pointless. Used to test Zorkish phase 1 spec.");
}

ZorkishGameplay::ZorkishGameplay(std::string WorldFile)
{
	World = new ZorkishWorld(WorldFile);
	Player = World->GetPlayer();
}

ZorkishGameplay::~ZorkishGameplay()
{
	delete World;
}

ZorkGameResult ZorkishGameplay::Play()
{
	std::cout << std::endl << "Welcome to Zorkish, " << World->GetWorldName() << std::endl;
	std::cout << World->GetWorldDescription() << std::endl << std::endl;

	Score.Adventure = World->GetWorldName();

	DescribeRoom();
	int NumTicks = 0;

	while (!QuitGame)
	{
		std::cout << std::endl;
		std::cout << ":> ";
		std::string input;
		std::getline(std::cin, input);
		ZorkishCommand* Command = Commands.InputCommand(input);

		if(Command != nullptr)
		{
			ZorkishCommandResult Result = Command->Run(Player);

			switch(Result)
			{
			case ZorkishCommandResult::EndGame:
				std::cout << "Your adventure has ended without fame or fortune." << std::endl;
				QuitGame = true;
				return ZorkGameResult::NoScore;

			case ZorkishCommandResult::HiScore:
				std::cout << "You have entered the magic word and will now see the \"New High Score\" screen." << std::endl;
				QuitGame = true;
				return ZorkGameResult::HighScore;

			default:
				break;
			}

			if(!QuitGame)
			{
				Message TickNum = Message("ticknum");
				TickNum.SetInt(NumTicks);
				NumTicks++;
				Blackboard::GetInstance()->PutMessage(&TickNum);

				Message TickMessage = Message("tick");
				Announcements.Announce(&TickMessage);


			}
		}
		else
		{
			std::cerr << "Unknown command: " << input << std::endl;
		}
	}

	return ZorkGameResult::NoScore; //Shouldn't be reachable yet.
}

ZorkishScore ZorkishGameplay::GetScore() const
{
	return Score;
}

void ZorkishGameplay::Help()
{
	ZorkishCommand* HelpObject = Commands.GetHelp();
	HelpObject->Run(Player);
}

void ZorkishGameplay::DescribeRoom()
{
	ZorkishRoom* CurrentRoom = Player->GetCurrentRoom();
	if(CurrentRoom != nullptr)
	{
		std::cout << "You are in: " << CurrentRoom->GetName() << std::endl;
		std::cout << CurrentRoom->GetDescription() << std::endl;
	}
}