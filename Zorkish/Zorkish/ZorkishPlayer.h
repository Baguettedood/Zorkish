#pragma once

#include "ZorkishEntity.h"
#include "ZorkishComposite.h"
class ZorkishRoom;

class ZorkishPlayer: public ZorkishEntity
{
public:
	ZorkishPlayer();
	~ZorkishPlayer();

	void SetCurrentRoom(ZorkishRoom* NewRoom);
	ZorkishRoom* GetCurrentRoom() { return CurrentRoom; }

protected:

private:
	ZorkishRoom* CurrentRoom = nullptr;

};