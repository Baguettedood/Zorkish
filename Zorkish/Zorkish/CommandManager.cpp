#include <iostream>
#include "CommandManager.h"
#include "ZorkishCommand.h"
#include "Commands/QuitCommand.h"
#include "Commands/HiScoreCommand.h"
#include "Commands/AddItemCommand.h"
#include "Commands/DestroyItemCommand.h"
#include "Commands/ExamineCommand.h"
#include "Commands/GoCommand.h"
#include "Commands/HelpCommand.h"
#include "Commands/TakeItemCommand.h"
#include "commands/AttackCommand.h"

CommandManager::CommandManager()
{
	AllCommands.push_back(new QuitCommand());
	AllCommands.push_back(new HiScoreCommand());
	AllCommands.push_back(new AddItemCommand());
	AllCommands.push_back(new DestroyItemCommand());
	AllCommands.push_back(new ExamineCommand());
	AllCommands.push_back(new GoCommand());
	AllCommands.push_back(new TakeItemCommand());
	AllCommands.push_back(new AttackCommand());


	Help = new HelpCommand(&AllCommands);
	AllCommands.push_back(Help);

	PopulateDictionary();
}

CommandManager::~CommandManager()
{
	for(ZorkishCommand* CommandToDelete : AllCommands)
	{
		delete CommandToDelete;
	}
}

ZorkishCommand* CommandManager::InputCommand(std::string FullCommand)
{
	ConvertToLowercase(FullCommand);

	//Find the actual command in this string
	std::string ActualCommandString = ToFirstSpace(FullCommand);

	try
	{
		ZorkishCommand* FoundCommand = CommandDictionary.at(ActualCommandString);
		FoundCommand->GiveCommand(FullCommand); //Let the command class deal with it. (Useful for single-character commands e.g. 'n')
		return FoundCommand;
	}
	catch(const std::exception&)
	{
		//Out-of-range exception is thrown if at() can't find the element.
		//The [] operator would create one or something...
		return nullptr;
	}
}

std::string CommandManager::ToFirstSpace(std::string GivenCommand)
{
	unsigned int SpacePosition = 0;
	for(SpacePosition = 0; SpacePosition < GivenCommand.length(); SpacePosition++)
	{
		if(GivenCommand[SpacePosition] == ' ')
			break;
	}

	if(SpacePosition == GivenCommand.length())
	{
		return GivenCommand;
	}
	else
	{
		std::string CommandOnly = GivenCommand.substr(0, SpacePosition);
		return CommandOnly;
	}
}

void CommandManager::ConvertToLowercase(std::string &StringToConvert)
{
	for(unsigned int i = 0; i < StringToConvert.length(); i++)
	{
		StringToConvert[i] = tolower(StringToConvert[i]);
	}
}

void CommandManager::PopulateDictionary()
{
	for(ZorkishCommand* ThisCommand : AllCommands)
	{
		AddSingleElement(ThisCommand->GetCommandName(), ThisCommand);

		//Also insert aliases
		std::vector<std::string> Aliases = ThisCommand->GetAliases();
		for(std::string Alias : Aliases)
		{
			AddSingleElement(Alias, ThisCommand);
		}
	}
}

void CommandManager::AddSingleElement(std::string Alias, ZorkishCommand* ThisCommand)
{
	ZorkishCommand* ExistingCommand = nullptr;
	try
	{
		ExistingCommand = CommandDictionary.at(Alias);

		//at() throws an exception, so it should only reach here if there is an element
		if(ExistingCommand != nullptr)
		{
			std::cerr << "Command " << ThisCommand->GetCommandName() << " with alias " << Alias
				<< " is already in use by " << ExistingCommand->GetCommandName() << std::endl;
		}
	}
	catch(const std::exception&) {}

	if(ExistingCommand == nullptr)
	{
		CommandDictionary[Alias] = ThisCommand;
	}

}
